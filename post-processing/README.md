<h1>ProSe-PA post-processing scripts</h1>

Post-processing R scripts are provided for analysing and plotting ProSe-PA simulations, either a simple forward simulation (__/ForwardSimulation__) , or a set of data assimilation output (__/DataAssimilation__)


